import React, { Component } from 'react';
import { Alert, StyleSheet, Text, View, ImageBackground, TouchableOpacity, TextInput, Image, Dimensions } from 'react-native';
import axios from 'axios'
import loginBG from '../image/loginBG.jpg'
import { Icon, InputItem, WhiteSpace, ActivityIndicator } from '@ant-design/react-native';
import { connect } from 'react-redux';


const { width: WIDTH } = Dimensions.get('window')
class Login extends Component {
    state = {
        email: '',
        password: '',
        isLoading: false
    }

    login = async () => {
        try {
            this.setState({ isLoading: true })
            const res = await axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.email,
                password: this.state.password
            })
            const user = res.data.user
            await this.props.userLogin(user)
            this.setState({ isLoading: false })
            await this.props.history.push('/Main')
        } catch (error) {
            this.setState({ isLoading: false })
            const err = error.response.data.errors ? error.response.data.errors : error
            console.log('login response', error.response.data.errors);
            let errorMessage = ''
            if (err.email) errorMessage += '- Email: ' + err.email + '\n'
            if (err.password) errorMessage += '- Password: ' + err.password + '\n'
            Alert.alert('Incorrect Login', errorMessage)
        }
    }

    signup = () => {
        this.props.history.push('/Register')
    }
    goToMain = () => {
        this.props.history.push('/Main')
    }
    change = () => {
        this.props.history.push('/ChangePassword')
    }

    render() {
        return (
            <ImageBackground source={loginBG} style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('../image/logo1.png')} style={{ width: 400, height: 200 }} />
                </View>
                <View style={{ left: 60, top: 10 }}>
                    <TouchableOpacity style={styles.change}
                        onPress={this.change}
                    >
                        <Text style={styles.textChange}>เปลี่ยนรหัสผ่าน</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ width: '80%', top: 8 }}>
                    <Icon style={styles.icon1} name='user' size={25} color='black' />
                    <InputItem
                        autoCapitalize={'none'}
                        value={this.state.email}
                        onChange={value => this.setState({ email: value })}
                        style={styles.input}
                        placeholder='Email'
                    />
                    <WhiteSpace size="xs" />
                    <Icon style={styles.icon2} name='lock' size={25} color='black' />
                    <InputItem
                        autoCapitalize={'none'}
                        type='password'
                        value={this.state.password}
                        onChange={value => { this.setState({ password: value }) }}
                        style={styles.input}
                        secureTextEntry={true}
                        placeholder='Password'
                    />
                </View>
                <View style={styles.bottom}>
                    <TouchableOpacity style={styles.btnLogin}
                        onPress={this.login}
                    >
                        <Text style={styles.textLogin}>เข้าสู่ระบบ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnRegister}
                        onPress={this.signup}
                    >
                        <Text style={styles.textLogin}>ลงทะเบียน</Text>
                    </TouchableOpacity>
                </View>

                {this.state.isLoading ?
                    <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', position: 'absolute', backgroundColor: 'rgba(255,255,255,0.7)' }}>
                        <ActivityIndicator size='large' color='orange'></ActivityIndicator>
                    </View> : <View style={{ flex: 0 }}></View>}

            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textChange: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        color: 'white'
    },
    change: {
        width: 90,
        height: 30,
        
    },
    logo: {
        alignItems: 'center',
        marginBottom: 20
    },
    inputContainer: {
        marginTop: 1,
    },
    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        width: WIDTH - 55,
        height: 45,
        borderRadius: 25,
        margin: 10,
        paddingHorizontal: 20,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: 'rgba(255, 255, 255, 1)',
    },
    icon1: {
        position: 'absolute',
        top: 8,
        left: -5
    },
    icon2: {
        position: 'absolute',
        top: 55,
        left: -5
    },
    btnLogin: {
        width: WIDTH - 250,
        height: 45,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 25,
        marginTop: 20,
        backgroundColor: '#3CB371',
        justifyContent: 'center'
    },
    btnRegister: {
        width: WIDTH - 250,
        height: 45,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 25,
        marginTop: 20,
        backgroundColor: '#FF7256',
        justifyContent: 'center'
    },
    textLogin: {
        fontFamily: 'Waffle Regular',
        color: 'white',
        fontSize: 30,
        textAlign: 'center'
    },
    bottom: {
        flexDirection: 'row'
    }

})

const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (user) => {
            dispatch({
                type: 'USER_LOGIN',
                user: user,
                // password: password
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(Login)