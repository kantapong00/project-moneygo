import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, Image, ImageBackground, Dimensions, TouchableHighlight, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { Icon, Card, InputItem } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'



const { width: WIDTH } = Dimensions.get('window')
class ChooseType extends Component {
    state = {
        catagory: '',
        description: '',
    }

    addNewTransaction = () => {
        let updateWallet = {}
        const todayDate = new Date()
        const newTransaction = {
            type: 'income',
            description: this.state.description,
            catagory: this.state.catagory,
            money: parseInt(this.state.money, 10)
        }
        if (this.props.selectwallet.transactions[0] && this.props.selectwallet.transactions[0].date) {
            if (this.formatDate(this.props.selectwallet.transactions[0].date) === this.formatDate(todayDate)) {
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance + newTransaction.money,
                    transactions: this.props.selectwallet.transactions
                }
                updateWallet.transactions[0].list.push(newTransaction)
            } else {
                const newTransactions = [{
                    date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                    list: [newTransaction]
                }, ...this.props.selectwallet.transactions]
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance + newTransaction.money,
                    transactions: newTransactions
                }
            }
        } else {
            const newTransactions = [{
                date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                list: [newTransaction]
            }, ...this.props.selectwallet.transactions]
            updateWallet = {
                ...this.props.selectwallet,
                balance: this.props.selectwallet.balance + newTransaction.money,
                transactions: newTransactions
            }
        }
        this.props.updateWallet(updateWallet)
        this.props.clickWallet(updateWallet)
        this.props.history.replace('/Revenue')
    }


    render() {
        return (
            <ImageBackground source={loginBG} style={styles.container}>
                <ScrollView style={styles.container}>
                    <View style={styles.type}>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/salary.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={value => { this.setState({ catagory: value }) }}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 130, top: -20 }}>เงินเดือน</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/give.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={value => { this.setState({ catagory: value }) }}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 120, top: -20 }}>พ่อ-แม่ให้มา</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/happy.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={value => { this.setState({ catagory: value }) }}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 135, top: -20 }}>เก็บได้</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <View style={styles.type2}>
                            <Card >
                                <Card.Body>
                                    <View style={{ height: 10, justifyContent: 'center' }}>
                                        <Image source={require('../image/search.png')} style={styles.icon2} />
                                        <TouchableHighlight
                                            onPress={value => { this.setState({ catagory: value }) }}
                                        >
                                            <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 70, top: -10 }}>อื่นๆ</Text>
                                        </TouchableHighlight>
                                    </View>
                                </Card.Body>
                            </Card>
                            <View>
                                <InputItem
                                    style={{ borderWidth: 1, borderColor: '#d7dae0', borderRadius: 6, paddingHorizontal: 14, height: 55, fontSize: 18 }}
                                    value={this.state.description}
                                    onChange={(value) => { this.setState({ description: value }) }}
                                    placeholder='Enter Description...' />

                            </View>
                            <View style={{ flexDirection: 'row', left: 77, top: 120 }}>
                                <TouchableOpacity style={styles.btnCheck}
                                    onPress={this.addNewTransaction}
                                >
                                    <Icon style={styles.icon1} name='check' size={20} color='white' />

                                </TouchableOpacity>

                                <TouchableOpacity style={styles.btnClose}
                                    onPress={this.gotoRevenue}
                                >
                                    <Icon style={styles.icon1} name='close' size={20} color='white' />
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground >

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    type: {
        top: 40,
        margin: 15,
    },
    type2: {
        top: -77,
        margin: 80,
        width: 130,
        right: 77
    },
    icon: {
        width: 45,
        height: 45,
        top: 18,
        left: 10,
    },
    icon2: {
        width: 45,
        height: 45,
        top: 27,
        left: 10,
    },
    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        width: WIDTH - 35,
        height: 80,
        paddingLeft: 20,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: 'rgba(255, 255, 255, 1)',
    },
    icon1: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    btnCheck: {
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#04af76',
        justifyContent: 'center',
    },
    btnClose: {
        marginLeft: 80,
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FF3333',
        justifyContent: 'center',
    },
})
export default ChooseType