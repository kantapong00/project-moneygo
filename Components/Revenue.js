import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, ImageBackground, TouchableHighlight, TextInput, Image, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import { Icon, Card, WingBlank, InputItem } from '@ant-design/react-native';
import DatePicker from 'react-native-datepicker'
import loginBG from '../image/loginBG.jpg'
import { element } from 'prop-types';


const { width: WIDTH } = Dimensions.get('window')

class Revenue extends Component {

    state = {
        catagory: '',
        description: '',
        money: '',
    }

    chooseType = () => {
        return this.props.history.push('/ChooseType')
    }
    gotoWallet = () => {
        return this.props.history.push('/Wallet')
    }
    gotoAddMoney = () => {
        return this.props.history.push('/Addmoney')
    }

    onChangeTypeSelect = (index, value) => {
        this.setState({ catagory: value })
    }

    formatDate = (date) => {
        function dateToString(date) {
            let d = date,
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }
        const today = new Date()
        if (dateToString(today) === dateToString(date))
            return 'วันนี้'
        return dateToString(date)
    }

    addNewTransaction = () => {
        let updateWallet = {}
        const todayDate = new Date()
        const newTransaction = {
            type: 'รายรับ',
            description: this.state.description,
            catagory: this.state.catagory,
            money: parseInt(this.state.money, 10)
        }
        if (this.props.selectwallet.transactions[0] && this.props.selectwallet.transactions[0].date) {
            if (this.formatDate(this.props.selectwallet.transactions[0].date) === this.formatDate(todayDate)) {
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance + newTransaction.money,
                    transactions: this.props.selectwallet.transactions
                }
                updateWallet.transactions[0].list.push(newTransaction)
            } else {
                const newTransactions = [{
                    date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                    list: [newTransaction]
                }, ...this.props.selectwallet.transactions]
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance + newTransaction.money,
                    transactions: newTransactions
                }
            }
        } else {
            const newTransactions = [{
                date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                list: [newTransaction]
            }, ...this.props.selectwallet.transactions]
            updateWallet = {
                ...this.props.selectwallet,
                balance: this.props.selectwallet.balance + newTransaction.money,
                transactions: newTransactions
            }
        }
        this.props.updateWallet(updateWallet)
        this.props.clickWallet(updateWallet)
        this.props.history.replace('/Addmoney')
    }



    render() {
        return (

            <View style={styles.container}>

                <ImageBackground source={loginBG} style={styles.top}>
                    <Image source={require('../image/revenue.png')} style={styles.image} />
                    <Text style={styles.revenueText}>รายรับ</Text>
                </ImageBackground>


                <View style={styles.bottom}>
             
                        <Image source={require('../image/coin.png')} style={styles.coin} />
                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 80, top: -7 }}>เงินที่ได้มา</Text>
                        <View style={{ top: -2 }}>
                            <InputItem
                                style={{ paddingHorizontal: 25, fontFamily: 'Waffle Regular', fontSize: 25, width: 50, height: 45, borderWidth: 1, borderColor: '#d7dae0', borderRadius: 30, justifyContent: 'center', textAlign: 'center' }}
                                type="number"
                                placeholder='กรอกจำนวนเงิน'
                                value={this.state.money}
                                onChange={value => { this.setState({ money: value }) }}

                            />
                        </View>

                        <View>
                            <Image source={require('../image/choose.png')} style={styles.choose} />
                            <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 100, top: -7 }}>ที่มาของรายรับ</Text>
                            <InputItem
                                style={{ paddingHorizontal: 25, fontFamily: 'Waffle Regular', fontSize: 25, width: 50, height: 45, borderWidth: 1, borderColor: '#d7dae0', borderRadius: 30, justifyContent: 'center', textAlign: 'center' }}
                                placeholder='กรอกที่มา'
                                value={this.state.catagory}
                                onChange={value => { this.setState({ catagory: value }) }}
                            />
                        </View>

                        <View style={{ flexDirection: 'row', left: 95, top: 70 }}>
                            <TouchableOpacity style={styles.btnCheck}
                                onPress={this.addNewTransaction}
                            >
                                <Icon style={styles.icon1} name='check' size={20} color='white' />

                            </TouchableOpacity>

                            <TouchableOpacity style={styles.btnClose}
                                onPress={this.gotoAddMoney}
                            >
                                <Icon style={styles.icon1} name='close' size={20} color='white' />
                            </TouchableOpacity>
                        </View>
           
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    date: {
        top: 15,
        left: 45
    },
    inputBox: {
        borderColor: '#aaaaaa',
        borderWidth: 1,
        borderRadius: 60
    },
    top: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFD700'
    },
    bottom: {
        flex: 2,
    },
    revenueText: {
        left: 80,
        top: -80,
        fontSize: 70,
        color: 'white',
        fontFamily: 'Waffle Regular',
    },
    image: {
        left: -90,
        width: 150,
        height: 150,
        top: 50
    },
    coin: {
        width: 25,
        height: 30,
        left: 48,
        top: 20
    },
    choose: {
        width: 60,
        height: 60,
        left: 35,
        top: 30
    },
    icon: {
        position: 'absolute',
        top: 25,
        left: 10
    },
    icon2: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        width: WIDTH - 120,
        height: 45,
        borderRadius: 25,
        margin: 10,
        paddingHorizontal: 20,
        borderColor: '#aaaaaa',
        borderWidth: 1,
    },
    text2: {
        fontFamily: 'Waffle Regular',
        fontSize: 30,
        top: 30,
        left: 50
    },
    icon1: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    btnCheck: {
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#04af76',
        justifyContent: 'center',
    },
    btnClose: {
        marginLeft: 80,
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FF3333',
        justifyContent: 'center',
    },

})

const mapStateToProps = (state) => {
    return {
        wallets: state.wallets,
        selectwallet: state.selectwallet
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateWallet: (newWallet) => {
            dispatch({
                type: 'UPDATE_WALLET',
                payload: newWallet
            })
        },
        clickWallet: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Revenue)
