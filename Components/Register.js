import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, ScrollView, Dimensions, Alert } from 'react-native';
import loginBG from '../image/loginBG.jpg'
import axios from 'axios'
import { InputItem } from '@ant-design/react-native';


const { width: WIDTH } = Dimensions.get('window')
class Register extends Component {

    state = {
        email: '',
        password: '',
        name: '',
        lastname: ''
    }

    userSignUp = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.name === '' || this.state.surname === '') {
            Alert.alert('Incorrect Input', 'Please do not leave input fields blanked')
        } else {
                try {
                    await axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.name,
                        lastName: this.state.lastname
                    })
                    await Alert.alert('Successed', 'Signing up was successful')
                    await this.props.history.replace('/')
                } catch (error) {
                    console.log('signup res', error.response.data.errors);
                }
            }
        }
        back = () => {
            return this.props.history.push('/Login')
        }

    render() {
        return (
            <ImageBackground source={loginBG} style={styles.container}>

                <View>
                    <Text style={{ fontFamily: 'Waffle Regular', fontSize: 60, top: -40 }}>ลงทะเบียน</Text>
                </View>
                <View style={{ width: '80%', top: -10 }}>
                    <Text style={styles.text}>อีเมล์ : </Text>
                    <InputItem
                        value={this.state.email}
                        onChange={value => this.setState({ email: value })}
                        style={styles.input}
                    />

                    <Text style={styles.text}>รหัสผ่าน : </Text>
                    <InputItem
                        type='password'
                        value={this.state.password}
                        onChange={value => { this.setState({ password: value }) }}
                        style={styles.input}
                    />

                    <Text style={styles.text}>ชื่อ : </Text>
                    <InputItem
                        value={this.state.name}
                        onChange={value => { this.setState({ name: value }) }}
                        style={styles.input}
                    />

                    <Text style={styles.text}>นามสกุล : </Text>
                    <InputItem
                        value={this.state.lastname}
                        onChange={value => { this.setState({ lastname: value }) }}
                        style={styles.input}
                    />

                    <View style={styles.bottom}>
                        <TouchableOpacity style={styles.btnLogin}
                            onPress={this.userSignUp}
                        >
                            <Text style={styles.textLogin}>ตกลง</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.btnBack}
                            onPress={this.back}
                        >
                            <Text style={styles.textLogin}>ยกเลิก</Text>

                        </TouchableOpacity>

                    </View>
                </View>

            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },

    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 30,
        borderWidth: 1,
        backgroundColor: 'white',
        borderRadius: 20,
        paddingHorizontal: 20,
    },
    text: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        marginTop: 10
    },
    btnLogin: {
        width: WIDTH - 250,
        height: 45,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 25,
        marginTop: 20,
        backgroundColor: '#54FF9F',
        justifyContent: 'center'
    },
    btnBack: {
        width: WIDTH - 250,
        height: 45,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 25,
        marginTop: 20,
        backgroundColor: '#FF7256',
        justifyContent: 'center'
    },
    textLogin: {
        fontFamily: 'Waffle Regular',
        color: 'white',
        fontSize: 30,
        textAlign: 'center'
    },
    bottom: {
        flexDirection: 'row',
        // right: 30,
        top: 30
    }
})
export default Register