
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native'
const radio_props = [
      { label: 'param1', value: 0 },
      { label: 'param2', value: 1 }
    ];
class test extends Component {
  render() {
    return (
      <View>
        <RadioForm
          radio_props={radio_props}
          onPress={(value) => { this.setState({ value: value }) }}
          buttonSize = {50}
          formHorizontal={true}
          selectedButtonColor={'#50C900'}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  // radio:{
  //   buttonColor: '#50C900'
  // }
})

export default test