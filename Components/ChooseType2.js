import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, Image, ImageBackground, Dimensions, TouchableHighlight, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { Icon, Card } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'



const { width: WIDTH } = Dimensions.get('window')
class ChooseType2 extends Component {

    gotoExpenses = () => {
        return this.props.history.push('/Expenses')
    }

    render() {
        return (
            <ImageBackground source={loginBG} style={styles.container}>
                <ScrollView style={styles.container}>
                    <View style={styles.type}>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/salary.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={this.chooseType}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 130, top: -20 }}>ค่าน้ำ-ค่าไฟ</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/car.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={this.chooseType}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 135, top: -20 }}>ค่าเดินทาง</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/food.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={this.chooseType}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 138, top: -20 }}>ค่าอาหาร</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/makeup.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={this.chooseType}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 118, top: -20 }}>ค่าเครื่องสำอางค์</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <Card style={{ margin: 2 }}>
                            <Card.Body>
                                <View style={{ height: 30, justifyContent: 'center' }}>
                                    <Image source={require('../image/shopping.png')} style={styles.icon} />
                                    <TouchableHighlight
                                        onPress={this.chooseType}
                                    >
                                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 142, top: -20 }}>ช้อปปิ้ง</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                        <View style={styles.type2}>
                            <Card >
                                <Card.Body>
                                    <View style={{ height: 10, justifyContent: 'center' }}>
                                        <Image source={require('../image/search.png')} style={styles.icon2} />
                                        <TouchableHighlight
                                            onPress={this.chooseType}
                                        >
                                            <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 70, top: -10 }}>อื่นๆ</Text>
                                        </TouchableHighlight>
                                    </View>
                                </Card.Body>
                            </Card>
                            <View>
                                <TextInput
                                    style={styles.input}
                                    placeholder='กรอกข้อความ'
                                />
                            </View>
                            <View style={{ flexDirection: 'row', left: 77, top: 50 }}>
                                <TouchableOpacity style={styles.btnCheck}
                                    onPress={this.gotoExpenses}
                                >
                                    <Icon style={styles.icon1} name='check' size={20} color='white' />

                                </TouchableOpacity>

                                <TouchableOpacity style={styles.btnClose}
                                    onPress={this.gotoExpenses}
                                >
                                    <Icon style={styles.icon1} name='close' size={20} color='white' />
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground >

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    type: {
        top: 40,
        margin: 15,
    },
    type2: {
        top: -77,
        margin: 80,
        width: 130,
        right: 77
    },
    icon: {
        width: 45,
        height: 45,
        top: 18,
        left: 10,
    },
    icon2: {
        width: 45,
        height: 45,
        top: 27,
        left: 10,
    },
    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        width: WIDTH - 35,
        height: 80,
        paddingLeft: 20,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: 'rgba(255, 255, 255, 1)',
    },
    icon1: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    btnCheck: {
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#04af76',
        justifyContent: 'center',
    },
    btnClose: {
        marginLeft: 80,
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FF3333',
        justifyContent: 'center',
    },
})
export default ChooseType2