import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ImageBackground, TouchableOpacity, TextInput, Image, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import { Icon, InputItem } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'


const { width: WIDTH } = Dimensions.get('window')
class EditWallet extends Component {

    goToMain = () => {
        return this.props.history.push('/Main')
    }

    goToAddMoney = () => {
        return this.props.history.push('/Addmoney')
    }

    render() {
        return (
            <ImageBackground source={loginBG} style={styles.container}>

                <TouchableOpacity style={styles.btnBack}
                    onPress={this.goToAddMoney}
                >
                    <Icon style={styles.icon} name='caret-left' size={20} color='white' />
                    <Text style={styles.textIcon1}>กลับ</Text>

                </TouchableOpacity>

                <Image source={require('../image/wallet.png')} style={styles.imageWallet} />

                <View style={styles.addBox}>
                    <TextInput style={{ left: 20, fontFamily: 'Waffle Regular', fontSize: 30, top: 2 }}
                        placeholder='ชื่อกระเป๋าเงิน'
                    />
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={styles.btnCheck}
                        onPress={this.goToAddMoney}
                    >
                        <Icon style={styles.icon1} name='edit' size={20} color='white' />
                        <Text style={styles.textIcon}>แก้ไข</Text>

                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnClose}
                        onPress={this.goToMain}
                    >
                        <Icon style={styles.icon1} name='delete' size={20} color='white' />
                        <Text style={styles.textIcon}>ลบกระเป๋า</Text>
                    </TouchableOpacity>
                </View>

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#8B4513',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnBack: {
        left: -120,
        top: -120,
        width: 95,
        height: 35,
        borderRadius: 60,
        // backgroundColor: '#FFD700',
        
    },
    icon: {
        position: 'absolute',
        top: 7,
        left: 15
    },
    textIcon1: {
        fontFamily: 'Waffle Regular',
        fontSize: 35,
        left: 40,
        top: -2,
        color: 'white'
    },
    addBox: {
        backgroundColor: 'white',
        width: 250,
        height: 50,
        borderRadius: 100,
        top: -60
    },
    imageWallet: {
        width: 200,
        height: 200,
        top: -50
    },
    icon1: {
        position: 'absolute',
        top: 10,
        left: 15
    },
    btnCheck: {
        top: -25,
        width: 100,
        height: 40,
        borderRadius: 60,
        backgroundColor: '#04af76',
        justifyContent: 'center',
    },
    btnClose: {
        marginLeft: 20,
        top: -25,
        width: 130,
        height: 40,
        borderRadius: 60,
        backgroundColor: '#FF3333',
        justifyContent: 'center',
    },
    textIcon: {
        fontFamily: 'Waffle Regular',
        fontSize: 35,
        left: 42,
        color: 'white'
    }
})
export default EditWallet