import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, TouchableOpacity, TouchableHighlight, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux'
import { Icon, TabBar, Card, Button, ActivityIndicator } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'
import profile from '../image/profile.png'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
import { push } from 'connected-react-router'



const { width: WIDTH } = Dimensions.get('window')
class Profile extends Component {

    state = {
        selectedTab: 'yellowTab',
        imagePath: '',
        isLoading: false
    }

    goToWallet = () => {
        return this.props.history.push('/Wallet')
    }
    goToMain = () => {
        return this.props.history.push('/Main')
    }
    goToProfile = () => {
        return this.props.history.push('/Profile')
    }

    selectImage = () => {

        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                this.setState({ isLoading: true })
                axios.post('https://zenon.onthewifi.com/moneyGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${this.props.user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image,
                            isLoading: false,
                        })
                    })
                    .catch(error => {
                        this.setState({ isLoading: false })
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        this.setState({ isLoading: true })
        axios.get('https://zenon.onthewifi.com/moneyGo/users', {
            headers: {
                Authorization: `Bearer ${this.props.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => {
                this.setState({ isLoading: false })
                console.log(err)
            })
            .finally(() => { console.log('Finally') })
    }

    logout = () => {
        return this.props.history.push('/Login')
    }
    editPage = () => {
        // this.props.addUser({});
        return this.props.history.push('/EditProfile')
    }



    render() {
        return (
            <View style={styles.container}>

                <ImageBackground source={loginBG} style={styles.top}>
                    <ImageBackground source={profile} style={{ width: 150, height: 150, top: 10, borderRadius: 80 }}>
                        <Image source={{ uri: this.state.imagePath }} style={{ width: 150, height: 150, borderRadius: 80 }} />
                    </ImageBackground>

                    {this.state.isLoading ?
                        <View
                            style={{
                                top: 37,
                                borderRadius: 80,
                                width: 150,
                                height: 150,
                                alignItems: 'center',
                                justifyContent: 'center',
                                position: 'absolute',
                                backgroundColor: 'rgba(255,255,255,0.7)'
                            }}>
                            <ActivityIndicator size='large' color='rgba(16,142,233,10.5)'></ActivityIndicator>
                        </View> : <View></View>}

                    <TouchableOpacity
                        style={{ width: '40%', left: 80, bottom: -15 }}
                        onPress={this.selectImage}
                    >
                        <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 18, top: -1, color: 'white' }}>แก้ไขรูปภาพโปรไฟล์</Text>
                    </TouchableOpacity>
                </ImageBackground>


                <View style={styles.body}>

                    <View style={styles.box}>
                        <Text style={styles.text}>โปรไฟล์ของคุณ</Text>
                    </View>

                    <View style={{ width: 320, left: 20, top: 10 }}>
                        <Card>
                            <Card.Body>
                                <View style={{ height: 20, justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 45, top: 22 }}>ชื่อ :</Text>
                                    <TouchableHighlight
                                        onPress={this.goToAddWallet}
                                    >
                                        <Text style={{ fontSize: 26, fontFamily: 'Waffle Regular', left: 100, top: -7 }}>{this.props.user.firstName}</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                    </View>
                    <View style={{ width: 320, left: 20, top: 10 }}>
                        <Card>
                            <Card.Body>
                                <View style={{ height: 20, justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 12, top: 22 }}>นามสกุล :</Text>
                                    <TouchableHighlight
                                        onPress={this.goToAddWallet}
                                    >
                                        <Text style={{ fontSize: 26, fontFamily: 'Waffle Regular', left: 100, top: -7 }}>{this.props.user.lastName}</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                    </View>
                    <View style={{ width: 319, left: 20, top: 10 }}>
                        <Card>
                            <Card.Body>
                                <View style={{ height: 20, justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 30, fontFamily: 'Waffle Regular', left: 35, top: 22 }}>อีเมล์ :</Text>
                                    <TouchableHighlight
                                        onPress={this.goToAddWallet}
                                    >
                                        <Text style={{ fontSize: 26, fontFamily: 'Waffle Regular', left: 100, top: -7 }}>{this.props.user.email}</Text>
                                    </TouchableHighlight>
                                </View>
                            </Card.Body>
                        </Card>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Button onPress={this.editPage} style={{ width: '40%', left: 20, top: 45 }} type='primary'>แก้ไขโปรไฟล์</Button>
                        <Button onPress={this.logout} style={{ width: '30%', left: 70, top: 45 }} type='warning'>ลงชื่อออก</Button>
                    </View>
                </View>
                <View style={styles.tabbar}>
                    <TabBar
                        unselectedTintColor="white"
                        tintColor="#FFFF00"
                        barTintColor="#f69173"
                    >
                        <TabBar.Item
                            title="หน้าหลัก"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === "blueTab"}
                            onPress={this.goToMain}
                        >

                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="wallet" />}
                            title="กระเป๋าเงิน"
                            selected={this.state.selectedTab === "redTab"}
                            onPress={this.goToWallet}
                        >

                        </TabBar.Item>
                        {/* <TabBar.Item

                            icon={<Icon name="setting" />}
                            title="ตั้งค่า"
                            selected={this.state.selectedTab === "greenTab"}

                        >

                        </TabBar.Item> */}
                        <TabBar.Item

                            icon={<Icon name="user" />}
                            title="ข้อมูล"
                            selected={this.state.selectedTab === "yellowTab"}
                            onPress={this.goToProfile}
                        >

                        </TabBar.Item>
                    </TabBar>
                </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    top: {
        flex: 0.8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CC6600'
    },
    body: {
        flex: 1.1
    },
    text2: {
        top: 20,
        left: 55,
        fontFamily: 'Waffle Regular',
        fontSize: 30,
    },
    box: {
        width: 500,
        height: 40,
        backgroundColor: '#f69173'
    },
    tabbar: {
        flex: 0.2
    },
    icon2: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    text: {
        right: 65,
        fontFamily: 'Waffle Regular',
        color: 'white',
        fontSize: 30,
        top: 3,
        textAlign: 'center'
    },
})
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps,{push})(Profile)
