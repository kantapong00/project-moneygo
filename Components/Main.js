import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ImageBackground, TouchableOpacity, TextInput, Image, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import { Icon, TabBar } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'



const { width: WIDTH } = Dimensions.get('window')
class Main extends Component {

    state = {
        selectedTab: 'blueTab',
        wallets: [],
        totalBalance: '',

    }


    UNSAFE_componentWillMount() {
        this.setState({ wallets: this.props.wallets })
    }

    componentDidMount = () => {
        this.getTotalBalance()
    }

    getTotalBalance = () => {
        let total = 0
        this.props.wallets.forEach(ele => {
            total += ele.balance
        })
        this.setState({ totalBalance: total })
        return total
    }




    goToWallet = () => {
        return this.props.history.push('/Wallet')
    }
    goToMain = () => {
        return this.props.history.push('/Main')
    }
    goToProfile = () => {
        return this.props.history.push('/Profile')
    }



    render() {
        return (
            <View style={styles.container}>

                <ImageBackground source={loginBG} style={styles.top}>

                    <Image source={require('../image/piggy.png')} style={{ width: 250, height: 150, top: 10 }} />
                    <View>
                        <Text style={styles.text}>ยอดเงินปัจจุบัน</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.money}>{parseFloat(this.state.totalBalance).toFixed(2)} THB</Text>
                    </View>
                </ImageBackground>
                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.resultBtn}>
                        <Text style={styles.text}>เคล็ดลับการออมเงิน</Text>
                    </View>
                </View>
                <ScrollView style={styles.body}>
                    <View style={{top:5}}>
                        <Image source={require('../image/10percent.png')} style={{ width: 110, height: 110, left: 20 }} />
                        <Text style={styles.info}>
                            1. เก็บก่อนใช้
                            เมื่อเงินเดือนออก แบ่ง 10% ของเงินเดือนเพื่อเป็นเงินออมทันที
                            และเงินก้อนนี้เพื่อการออมอย่างเดียวเท่านั้น
                            ห้ามเอาออกมาใช้เด็ดขาด!!
                        </Text>
                    </View>
                    <View style={{top:15}}>
                        <Image source={require('../image/less-money.png')} style={{ width: 85, height: 100, left: 20 }} />
                        <Text style={styles.info}>
                            2. พกเงินน้อยลง
                            พกเงินติดตัวจำนวนน้อยกว่าที่เคย อาจจะใช้ระบบการคำนวณค่าใช้จ่ายรายวัน
                            ว่าเราใช้เงินต่อสัปดาห์เท่าไหร่ แล้วพกพอดีเท่านั้น
                            และคอยเตือนตัวเองว่านี่คือเงินที่เราต้องใช้ทั้งสัปดาห์นะ
                            ไม่ใช่ใช้หมดใน 1 วัน
                            แล้วถอนเงินเป็นรายสัปดาห์แทนที่จะถอนเมื่อเงินหมด
                        </Text>
                    </View>
                    <View style={{top:25}}>
                        <Image source={require('../image/credit.png')} style={{ width: 90, height: 80, left: 20 }} />
                        <Text style={styles.info}>
                            3. งดใช้บัตรเครดิต
                            ใช้บัตรเครดิตเพื่อการชำระค่าใช้จ่ายรายเดือน เช่น ค่าเช่า ค่าน้ำ ค่าไฟ ค่าโทรศัพท์ ผ่อนสินเชื่อ
                            หรือหากต้องซื้อสินค้าชิ้นใหญ่ ราคาแพง เช่นต้องซื้อตู้เย็นใหม่ เครื่องซักผ้า ฯลฯ ถึงใช้บัตรเครดิต
                        </Text>
                    </View>
                    <View style={{top:35}}>
                        <Image source={require('../image/coin1.png')} style={{ width: 100, height: 80, left: 20 }} />
                        <Text style={styles.info}>
                            4. มีเศษเหรียญ ให้หยอดกระปุก
                            คอยสำรวจกระเป๋าสตางค์ของเราว่า มีเศษเหรียญอยู่บ้างไหม ถ้ามีก็อย่ารีรอ 
                            รีบหยอดกระปุกกุ๊กกิ๊กของเราให้เต็มเร็วๆ กันดีกว่า หรือถ้าวันไหนเกิดอยากจะให้โบนัสตัวเอง 
                            ก็ลองเพิ่มจากเศษเหรียญเป็นแบงก์ 20 บ้างก็ได้ พอกระปุกเต็มเมื่อไหร่ก็ลองแกะมานับดู 
                            เผลอ ๆ ได้มาอีกหลายร้อยโดยไม่รู้ตัวนะเอ้า!
                        </Text>
                    </View>
                    
                        <Image source={require('../image/aom.png')} style={{ width: 250, height: 230, left: 12, margin: 50 }} />
                        
                    
                </ScrollView>
                <View style={styles.tabbar}>
                    <TabBar
                        unselectedTintColor="white"
                        tintColor="#FFFF00"
                        barTintColor="#f69173"
                    >
                        <TabBar.Item
                            title="หน้าหลัก"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === "blueTab"}
                            onPress={this.goToMain}
                        >

                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="wallet" />}
                            title="กระเป๋าเงิน"
                            selected={this.state.selectedTab === "redTab"}
                            onPress={this.goToWallet}
                        >

                        </TabBar.Item>
                        {/* <TabBar.Item

                            icon={<Icon name="setting" />}
                            title="ตั้งค่า"
                            selected={this.state.selectedTab === "greenTab"}

                        >

                        </TabBar.Item> */}
                        <TabBar.Item

                            icon={<Icon name="user" />}
                            title="ข้อมูล"
                            selected={this.state.selectedTab === "yellowTab"}
                            onPress={this.goToProfile}
                        >

                        </TabBar.Item>
                    </TabBar>
                </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    top: {
        flex: 0.8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CC6600'
    },
    info: {
        top: 10,
        left: 5,
        fontSize: 25,
        fontFamily: 'Waffle Regular',
    },
    money: {
        marginTop: -15,
        fontSize: 50,
        color: 'white',
        fontFamily: 'Waffle Regular',
    },
    pie: {
        marginLeft: 70,
        flexDirection: 'row',
        top: 2,
        left: 60
    },
    tabbar: {
        flex: 0.2
    },
    body: {
        flex: 1.1
    },

    resultBtn: {
        flex: 1,
        margin: 1.5,
        flexDirection: 'row',
        backgroundColor: '#f69173',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },

    icon: {
        position: 'absolute',
        top: 25,
        left: 10
    },
    icon2: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    btnWallet: {
        top: -10,
        width: WIDTH - 200,
        height: 80,
        borderRadius: 60,
        backgroundColor: '#FFD700',
        justifyContent: 'center',
    },
    btnOption: {
        top: 5,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FFD700',
        justifyContent: 'center',
    },
    btnProfile: {
        top: 5,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FFD700',
        justifyContent: 'center',
    },
    text: {
        left: 5,
        fontFamily: 'Waffle Regular',
        color: 'white',
        fontSize: 30,
        textAlign: 'center'
    },
})
const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        goToWalletPage: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        },
        replace: (path) => {
            dispatch(replace(path))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Main)
