import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ImageBackground, TouchableOpacity, Alert, Image, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import { Icon, InputItem } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'


const { width: WIDTH } = Dimensions.get('window')
class addWallet extends Component {

    state = {
        walletname: '',
        walletmoney: '0.00'
    }

    gotoWallet = () => {
        return this.props.history.push('/Wallet')
    }

    saveToMainPage = () => {
        let newId = 0
        if (this.props.wallets.length > 0)
            newId = parseInt(this.props.wallets[this.props.wallets.length - 1]._id, 10) + 1
        const wallet = {
            _id: newId.toString(),
            name: this.state.walletname === '' ? 'Wallet Name' : this.state.walletname,
            balance: this.state.walletmoney === '' ? 0.00 : parseFloat(this.state.walletmoney),
            transactions: []
        }
        this.props.addWallet(wallet)
        Alert.alert('เพิ่มกระเป๋าใหม่', 'สำเร็จ')
        return this.props.history.push('/Wallet')
    }



    render() {
        return (
            <ImageBackground source={loginBG} style={styles.container}>
              
                <Image source={require('../image/wallet.png')} style={styles.imageWallet} />

                <View style={{ width: '80%', top: -50 }}>
                    <InputItem
                        value={this.state.walletname}
                        onChange={value => { this.setState({ walletname: value }) }}
                        style={styles.input}
                        placeholder='ชื่อกระเป๋าเงิน'
                    />
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={styles.btnCheck}
                        onPress={this.saveToMainPage}
                    >
                        <Icon style={styles.icon1} name='check' size={20} color='white' />

                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnClose}
                        onPress={this.gotoWallet}
                    >
                        <Icon style={styles.icon1} name='close' size={20} color='white' />
                    </TouchableOpacity>
                </View>

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#8B4513',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 25,
        width: WIDTH - 55,
        height: 45,
        borderRadius: 25,
        paddingHorizontal: 20,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: 'rgba(255, 255, 255, 1)',
    },
    imageWallet: {
        width: 200,
        height: 200,
        top: -50
    },
    icon1: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    btnCheck: {
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#04af76',
        justifyContent: 'center',
    },
    btnClose: {
        marginLeft: 80,
        top: -25,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FF3333',
        justifyContent: 'center',
    },
})

const mapStateToProps = (state) => {
    return {
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addWallet: (wallet) => {
            dispatch({
                type: 'ADD_WALLET',
                payload: wallet
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(addWallet)


