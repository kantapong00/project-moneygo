import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, TouchableOpacity, TouchableHighlight, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux'
import { Icon, InputItem, Card, Button, ActivityIndicator } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'
import { push } from 'connected-react-router'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'



const { width: WIDTH } = Dimensions.get('window')
class EditProfile extends Component {

    state = {
        firstName: '',
        lastName: '',
    }

    goToProfile = () => {
        return this.props.history.push('/Profile')
    }

    onSave = () => {
        axios({
            url: 'https://zenon.onthewifi.com/moneyGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${this.props.user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/moneyGo/users', {
                headers: {
                    'Authorization': `Bearer ${this.props.user.token}`
                },
            }
            )
        })
            .then(response => {
                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/Profile', {
                    item: 'profile'
                })
            }).catch(e => {
                console.log("error " + e)
            })

    }


    render() {
        const { user, editUser } = this.props
        return (
            <ImageBackground source={loginBG} style={styles.container}>
                <View>
                    <Text style={styles.textHead}>แก้ไขข้อมูลโปรไฟล์</Text>
                </View>
                <View style={{ width: '80%' }}>
                    <Text style={styles.text}>ชื่อ : </Text>
                    <InputItem
                        value={this.state.firstName}
                        onChange={value => { this.setState({ firstName: value }) }}
                        style={styles.input}
                    />

                    <Text style={styles.text}>นามสกุล : </Text>
                    <InputItem
                        value={this.state.lastName}
                        onChange={value => { this.setState({ lastName: value }) }}
                        style={styles.input}
                    />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Button
                        style={styles.buttons}
                        onPress={() => this.onSave()}
                        color="white"
                        type="primary"
                    >
                        <Icon name="save" size="sm" color="white" /> บันทึก
                    </Button>

                    <Button
                        style={styles.buttons}
                        onPress={this.goToProfile}
                        color="white"
                        type="warning"
                    >
                        <Icon name='arrow-left' size="sm" color='white' /> ยกเลิก
                    </Button>

                </View>

            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        fontFamily: 'Waffle Regular',
        fontSize: 30,
        borderWidth: 1,
        backgroundColor: 'white',
        borderRadius: 20,
        paddingHorizontal: 20,
    },
    text: {
        fontFamily: 'Waffle Regular',
        fontSize: 30,
        marginTop: 10,
        color: 'white'
    },
    textHead: {
        fontFamily: 'Waffle Regular',
        fontSize: 45,
        marginTop: 12,
        color: 'white'
    },
    buttons: {
        top: 15,
        marginLeft: 15
    },
    btnBack: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        fontFamily: 'Waffle Regular',
        fontSize: 40,
    },

})
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)
