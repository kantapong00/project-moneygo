import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, ImageBackground, TouchableHighlight, TouchableOpacity, Alert, Image, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import { Icon, TabBar, List, SwipeAction } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'
import { push } from 'connected-react-router'


const Item = List.Item;
const { width: WIDTH } = Dimensions.get('window')
class wallet extends Component {

    state = {
        selectedTab: 'redTab',
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }

    UNSAFE_componentWillMount() {
        console.log('props:', this.props);
        console.log('FETCH WALLETS:', this.props.wallets);
        this.setState({ wallets: this.props.wallets })
    }


    goToAddWallet = () => {
        return this.props.history.push('/AddWallet')
    }
    goToMain = () => {
        return this.props.history.push('/Main')
    }
    goToProfile = () => {
        return this.props.history.push('/Profile')
    }
    goToAddmoney = (wallet) => {
        this.props.goToWalletPage(wallet)
        this.props.push('/Addmoney')
    }


    render() {
        return (
            <View style={styles.container}>

                <ImageBackground source={loginBG} style={styles.top}>

                    <Image source={require('../image/wallet1.png')} style={{ width: 250, height: 250, marginTop: 15 }} />

                    {/* <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.money}>0.00</Text>
                    </View> */}
                </ImageBackground>
                <View style={styles.resultBtn}>
                    <View style={{ width: 160, left: 1, top: -2 }}>
                        <Icon name="plus-circle" style={{ top: 20, left: 10, color: 'white' }} />
                        <TouchableOpacity
                            onPress={this.goToAddWallet}
                        >
                            <Text style={{ fontSize: 35, fontFamily: 'Waffle Regular', left: 40, top: -10, color: 'white' }}>Add Wallet</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.walletBox}>
                <ScrollView>
                            <FlatList
                                data={this.state.wallets}
                                style={{ width: '100%' }}
                                keyExtractor={(item) => item._id}
                                renderItem={({ item }) => (
                                        <Item
                                            extra={Number.parseFloat(item.balance).toFixed(2)}
                                            style={styles.wallet}
                                            arrow="horizontal"
                                            onPress={() => this.goToAddmoney(item)}>

                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ fontFamily: 'Waffle Regular', fontSize: 30, left: 25 }}>{item.name}</Text>
                                                {/* <Text style={{ fontSize: 24 }}>{Number.parseFloat(item.balance).toFixed(2)}</Text> */}
                                            </View>
                                        </Item>
                                )}
                            />
                        </ScrollView>

                </View>
                <View style={styles.tabbar}>
                    <TabBar
                        unselectedTintColor="white"
                        tintColor="#FFFF00"
                        barTintColor="#f69173"
                    >
                        <TabBar.Item
                            title="หน้าหลัก"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === "blueTab"}
                            onPress={this.goToMain}
                        >

                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="wallet" />}
                            title="กระเป๋าเงิน"
                            selected={this.state.selectedTab === "redTab"}
                            onPress={this.goToWallet}
                        >

                        </TabBar.Item>
                        {/* <TabBar.Item

                            icon={<Icon name="setting" />}
                            title="ตั้งค่า"
                            selected={this.state.selectedTab === "greenTab"}

                        >

                        </TabBar.Item> */}
                        <TabBar.Item

                            icon={<Icon name="user" />}
                            title="ข้อมูล"
                            selected={this.state.selectedTab === "yellowTab"}
                            onPress={this.goToProfile}
                        >

                        </TabBar.Item>
                    </TabBar>
                </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    top: {
        flex: 1.8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CC6600'
    },
    money: {
        marginTop: -20,
        fontSize: 70,
        color: 'white',
        fontFamily: 'Waffle Regular',
    },
    walletBox: {
        flex: 2,
        top: 5,
        left: 10,
        // alignItems: 'center',
        width: 340,
    },
    resultBtn: {
        flex: 0.3,
        margin: 1,
        flexDirection: 'row',
        backgroundColor: '#5c979b',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabbar: {
        flex: 0.4
    },
    icon: {
        position: 'absolute',
        top: 25,
        left: 10
    },
    icon2: {
        position: 'absolute',
        top: 15,
        left: 15
    },
    btnWallet: {
        top: -10,
        width: WIDTH - 200,
        height: 80,
        borderRadius: 60,
        backgroundColor: '#FFD700',
        justifyContent: 'center',
    },
    btnOption: {
        top: 5,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FFD700',
        justifyContent: 'center',
    },
    btnProfile: {
        top: 5,
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: '#FFD700',
        justifyContent: 'center',
    },
    text: {
        fontFamily: 'Waffle Regular',
        color: 'white',
        fontSize: 30,
        textAlign: 'center'
    },
    wallet: {
        right: 5,
        height: 40,
        width: 340,
        borderColor: '#aaaaaa',
        borderWidth: 1,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',

        margin: 5
    }

})
const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        goToWalletPage: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        },
        push: (path) => {
            dispatch(push(path))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(wallet)

