import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, ScrollView, Image, ImageBackground, TouchableOpacity, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux'
import { Icon, Card, List } from '@ant-design/react-native';
import loginBG from '../image/loginBG.jpg'


const Item = List.Item;
class Addmoney extends Component {

    state = {
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }

    UNSAFE_componentWillMount = () => {
        console.log('wallet props', this.props.selectwallet);
        let selected = this.props.wallets.find(ele => { return ele._id === this.props.selectwallet._id })
        this.setState({ wallet: selected })
    }

    getIncomePerDay = (list) => {
        let income = 0;
        list.forEach(ele => {
            if (ele.type === 'รายรับ') income += ele.money
        })
        return income
    }

    getExpensePerDay = (list) => {
        let expense = 0;
        list.forEach(ele => {
            if (ele.type === 'รายจ่าย') expense += ele.money
        })
        return expense
    }

    formatDate = (date) => {
        function dateToString(date) {
            let d = date,
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }
        const today = new Date()
        if (dateToString(today) === dateToString(date))
            return 'วันนี้'
        return dateToString(date)
    }


    goToWallet = () => {
        return this.props.history.push('/Wallet')
    }
    gotoRevenue = () => {
        return this.props.history.push('/Revenue')
    }
    goToEdit = () => {
        return this.props.history.push('/EditWallet')
    }
    goToExpenses = () => {
        return this.props.history.push('/Expenses')
    }



    render() {
        return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <TouchableOpacity style={styles.btnBack}
                        onPress={this.goToWallet}
                    >
                        <Icon name='arrow-left' size={25} color='white' />
                        <Text style={{
                            fontFamily: 'Waffle Regular',
                            fontSize: 30,
                            color: 'white'
                        }}>  กลับ </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnEdit}
                        onPress={this.goToEdit}
                    >
                        <Icon name='edit' size={25} color='white' />
                        <Text style={{
                            fontFamily: 'Waffle Regular',
                            fontSize: 30,
                            color: 'white'
                        }}>  แก้ไข </Text>
                    </TouchableOpacity>
                </View>

                <ImageBackground source={loginBG} style={styles.top}>
                    <Image source={require('../image/moneybag.png')} style={styles.imageWallet} />
                    <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', left: 68 }}>
                        <Text style={styles.textTop}>{this.state.wallet.name}</Text>
                        <Text style={styles.textTop2}>{parseFloat(this.state.wallet.balance).toFixed(2)}</Text>
                    </View>
                </ImageBackground>

                <View style={styles.body}>
                    <ScrollView>
                        <FlatList
                            data={this.state.wallet.transactions.reverse()}
                            renderItem={({ item }) => (
                                <List>
                                    <View style={{ width: '100%' }}>
                                        <View style={{ width: '100%', height: 36, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                                            <Text>{this.formatDate(item.date)}</Text>
                                            <Text style={{fontFamily: 'Waffle Regular', fontSize: 25, color: '#2E8B57'}}>รายรับรวม: {parseFloat(this.getIncomePerDay(item.list)).toFixed(2)}</Text>
                                            <Text style={{fontFamily: 'Waffle Regular', fontSize: 25, color: '#CD5C5C'}}>รายจ่ายรวม: {parseFloat(this.getExpensePerDay(item.list)).toFixed(2)}</Text>
                                        </View>
                                        <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.15)' }} />
                                    </View>

                                    <Item style={styles.Item} >
                                        {item.list.map((ele) => {
                                            return (
                                                <Item style={{ flexDirection: 'row', }} >
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                        <Text style={{fontFamily: 'Waffle Regular', fontSize: 38,}}>{ele.type}</Text>
                                                        <Text style={{fontFamily: 'Waffle Regular', fontSize: 20, top: 10}}>{ele.catagory}</Text>
                                                        <Text style={{fontFamily: 'Waffle Regular', fontSize: 38,}}>{parseFloat(ele.money).toFixed(2)}</Text>
                                                    </View>

                                                </Item>
                                            )
                                        })}
                                    </Item>
                                </List>
                            )}
                        />
                    </ScrollView>


                </View>
                <View style={{ flex: 0.8, flexDirection: 'row', backgroundColor: '#f69173' }}>
                    <TouchableOpacity style={styles.btnPlus1}
                        onPress={this.gotoRevenue}
                    >
                        <Icon style={styles.icon} name='plus' size={20} color='white' />
                        <Text style={styles.textPlus}>รายรับ</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.btnPlus2}
                        onPress={this.goToExpenses}
                    >
                        <Icon style={styles.icon} name='plus' size={20} color='white' />
                        <Text style={styles.textPlus}>รายจ่าย</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 0.4,
        flexDirection: 'row',
        backgroundColor: '#f69173'
    },
    top: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: '#FFEFDB'
    },
    textTop: {
        fontFamily: 'Waffle Regular',
        fontSize: 38,
        width: 160,
        height: 50,
        // backgroundColor: 'blue'
    },
    textTop2: {
        fontFamily: 'Waffle Regular',
        fontSize: 50,
        width: 160,
        height: 60,
        // backgroundColor: 'green'
    },
    imageWallet: {
        width: 130,
        height: 130,
        top: 18,
        left: 20
    },
    btnBack: {
        flex: 1,
        flexDirection: 'row',
        top: -7,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textHead: {
        flex: 1,
        justifyContent: 'center',
        top: 5,
        left: 15,
        fontSize: 30,
        color: 'white',
        fontFamily: 'Waffle Regular',
    },
    btnEdit: {
        flex: 1,
        flexDirection: 'row',
        top: -7,
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        justifyContent: 'center',
    },
    body: {
        flex: 4
    },
    btnPlus1: {
        marginLeft: 50,
        top: 15,
        width: 110,
        height: 40,
        borderRadius: 60,
        backgroundColor: '#04af76',
        justifyContent: 'center',
    },
    btnPlus2: {
        left: 35,
        top: 15,
        width: 110,
        height: 40,
        borderRadius: 60,
        backgroundColor: '#FF3333',
        justifyContent: 'center',
    },
    icon: {
        position: 'absolute',
        top: 10,
        left: 10,
    },
    textPlus: {
        fontFamily: 'Waffle Regular',
        fontSize: 30,
        left: 42,
        color: 'white'
    }
})
const mapStateToProps = (state) => {
    return {
        selectwallet: state.selectwallet,
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {

}

export default connect(mapStateToProps)(Addmoney)
