/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Router from './system/Router';
import Login from './Components/Login'
import AddWallet from './Components/AddWallet'
import Main from './Components/Main';
import MyDatePicker from './Components/date';
import Addmoney from './Components/Addmoney';
import date from './Components/date';
import EditWallet from './Components/EditWallet';
import Revenue from './Components/Revenue'
import wallet from './Components/Wallet';
import ChooseType from './Components/ChooseType';
import Expenses from './Components/Expenses';
import ChooseType2 from './Components/ChooseType2';
import Profile from './Components/Profile';




AppRegistry.registerComponent(appName, () => Router);
