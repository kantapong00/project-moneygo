import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-native'
import { store, history } from './Store'
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux'


import Login from '../Components/Login';
import Register from '../Components/Register';
import Main from '../Components/Main';
import AddWallet from '../Components/AddWallet';
import Addmoney from '../Components/Addmoney';
import Wallet from '../Components/Wallet';
import Revenue from '../Components/Revenue';
import ChooseType from '../Components/ChooseType'
import EditWallet from '../Components/EditWallet'
import Expenses from '../Components/Expenses'
import ChooseType2 from '../Components/ChooseType2'
import Profile from '../Components/Profile'
import EditProfile from '../Components/EditProfile'
import ChangePassword from '../Components/ChangePassword'



class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route exact path="/Register" component={Register} />
                        <Route exact path="/Main" component={Main} />
                        <Route exact path="/AddWallet" component={AddWallet} />
                        <Route exact path="/Addmoney" component={Addmoney} />
                        <Route exact path="/EditWallet" component={EditWallet} />
                        <Route exact path="/Wallet" component={Wallet} />
                        <Route exact path="/Revenue" component={Revenue} />
                        <Route exact path="/ChooseType" component={ChooseType} />
                        <Route exact path="/Expenses" component={Expenses} />
                        <Route exact path="/ChooseType2" component={ChooseType2} />
                        <Route exact path="/Profile" component={Profile} />
                        <Route exact path="/EditProfile" component={EditProfile} />
                        <Route exact path="/ChangePassword" component={ChangePassword} />
                        <Redirect to="/" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}



export default Router